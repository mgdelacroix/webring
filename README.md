# Webring
Este webring es un intento para inspirar a la gente a crear y mantener sus propios sitios personales y compartir tráfico entre ellos.
El objetivo es compartir sitios «hechos a mano» como por ejemplo blogs, wikis o portfolios.

## Unirse al webring
```html
<a href="https://webring.tr4ck.net/#tu-id-aqui" target="_blank" rel="noopener">
  <img src="https://webring.tr4ck.net/icono.svg" alt="tr4ck webring"/>
</a>
```

1. Agrega el icono del webring al HTML de tu página web usando el código anterior.
2. Añade la información de tu página web al archivo index.html. Intenta mantener el nombre de tu link corto, y no dejes una `/` al final en el atributo `href`. Usa un valor alfanumérico con sentido para el atributo `id`.
3. Envía un «pull request» con la url a tu sitio web en la descripción. Los «pull request» si descripción serán rechazados. Otra opción es comentar en la «issue» abierta para tal efecto.

### Enlace circular
En lugar de enlazar con el directorio, también puedes enlazar con el siguiente del anillo, añadiendo su id en la url. Por ejemplo:

```html
<a href="https://webring.tr4ck.net/#serxoz" target="_blank" rel="noopener noreferrer">
  <img src="https://webring.tr4ck.net/icono.svg" alt="tr4ck webring"/>
</a>
```

